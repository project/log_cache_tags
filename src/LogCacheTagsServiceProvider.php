<?php

namespace Drupal\log_cache_tags;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

class LogCacheTagsServiceProvider extends ServiceProviderBase Implements ServiceProviderInterface {
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('cache_tags.invalidator');
    $definition->setClass('Drupal\log_cache_tags\MyInvalidateTags\MyInvalidateTags');
  }

}
