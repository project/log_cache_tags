<?php

namespace Drupal\log_cache_tags\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LogCacheTagsForm.
 */
class LogCacheTagsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'log_cache_tags.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'log_cache_tags_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('log_cache_tags.settings');

    $form['log_cache_tags'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log Cache Tag Invalidations'),
      '#description' => $this->t('Check this to begin logging cache tag usage across the site.'),
      '#default_value' => $config->get('log_cache_tags'),
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      \Drupal::configFactory()->getEditable('log_cache_tags.settings')->set($key, $value)->save();
    }
  }

}
