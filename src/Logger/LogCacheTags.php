<?php

namespace Drupal\log_cache_tags\Logger;

use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LoggerInterface;

/**
 * Class LogCacheTagsToQueue.
 *
 * @package Drupal\log_cache_tags\Logger
 */
class LogCacheTagsToQueue implements LoggerInterface {
  use RfcLoggerTrait;

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    $config = \Drupal::config('log_cache_tags.settings');
    $purge_add = $config->get('log_cache_tags');

    if ($purge_add != FALSE) {
      // Log each time we send a record to the purge_queue_alt table.
      $date = date('m/d/Y h:i:sa', time());
      $output = $date . ': ' . $message;
    } else {
      $output = NULL;
    }

    return $output;
  }

}
